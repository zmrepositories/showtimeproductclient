package nl.hanze.movieshowtime.response;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="Response")
public class Message
{

    public static final String SUCCESS = "SUCCESS";


    public static final String FAILED = "FAILED";


    public static final String INFO = "INFO";


    private Long id = -1L;


    private String message;


    private String title;


    private List<Fault> errors;


    private List<Success> successes;


    public Message()
    {
        errors      = new ArrayList<Fault>();
        successes   = new ArrayList<Success>();
    }


    public Message(String title, String message)
    {
        this();
        setTitle(title);
        setText(message);
    }


    public Message setId(long id)
    {
        this.id = id;
        return this;
    }


    @XmlElement(name="id")
    public long getId()
    {
        return id;
    }



    public Message setTitle(String title)
    {
        this.title = title;
        return this;
    }


    @XmlElement(name="title")
    public String getTitle()
    {
        return title;
    }



    @XmlElementWrapper(name="failed")
    @XmlElement(name="fault")
    public List<Fault> getFault()
    {
        return errors;
    }


    @XmlElementWrapper(name="created")
    @XmlElement(name="success")
    public List<Success> getSuccesses()
    {
        return successes;
    }





    public Message setText(String message)
    {
        this.message = message;
        return this;
    }


    /**
     * @return the content of the message
     */
    @XmlElement(name = "message")
    public String getText() {
        return message;
    }


    /**
     * sets title of the message to succes
     */
    public Message success() {
        this.setTitle(SUCCESS);
        return this;
    }

    /**
     * sets title of the message to failed
     */
    public Message failed() {
        this.setTitle(FAILED);
        return this;
    }

    /**
     *
     * @param fault Fault
     * @return Message
     */
    public Message addFaultItem(Fault fault)
    {
        errors.add(fault);
        return this;
    }

    /**
     *
     * @param success Success
     * @return Message
     */
    public Message addSuccessItem(Success success)
    {
        successes.add(success);
        return this;
    }

    /**
     * returns a nicely formatted message
     *
     * @see Object#toString()
     */
    public String toString()
    {
        String successStr = "";
        String errorStr   = "";

        for(Success success : successes){
            successStr += success;
        }
        for(Fault fault : errors){
            errorStr += fault;
        }
        return String.format("Message:[\n " +
                "\tid: %s " +
                "\n\ttitle: %s " +
                "\n\tmessage: %s " +
                "%s"+
                "%s"+
                "\n]",getId(), getTitle() ,getText(), ((successStr.isEmpty())? "": successStr), ((errorStr.isEmpty())?   "": errorStr));
    }
}
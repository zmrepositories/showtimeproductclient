package nl.hanze.movieshowtime.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity(name="category")
@Table(name="category")
@XmlRootElement(name="category")
public class Category implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;



    @Column(name = "category_id")
    private long categoryId;


    @Column(name = "name")
    private String name;



    @XmlElement(name = "id")
    public long getId() {
        return id;
    }


    public void setId(long id) {
        this.id = id;
    }


    @XmlElement(name = "categoryId")
    public long getCategoryId() {
        return categoryId;
    }


    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }


    @XmlElement(name = "name")
    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        if (categoryId != category.categoryId) return false;
        if (id != category.id) return false;
        if (name != null ? !name.equals(category.name) : category.name != null) return false;

        return true;
    }


    @Override
    public int hashCode() {
        int result = Integer.parseInt(((Long)id).toString());
        result = 31 * result + Integer.parseInt(((Long)categoryId).toString());
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }


    public boolean exists()
    {
        return getId() > 0;
    }


    public String toString()
    {
        return String.format("\t  Category: [\n " +
                        "\t      Id: %s " +
                        "\n\t      CategoryId: %s " +
                        "\n\t      Name: %s " +
                "\n    ]",getId() ,getCategoryId(), getName());
    }
}

# ShowTime Product Client
 This client is written to do crud action easier on te services application
it's uses the domain entities to work with

### Features
* maven project
* uses Jersey frame work



## important

notice set in the header the flowing information anny option is possible get json retrieve
xml or reverse or both types. Set the _Accept_ and _Content-Type_ in wat you like
```sh
Accept: application/json
Content-Type: application/json
```
or
```sh
Accept: application/xml
Content-Type: application/xml
```



## URL's

### GET: get all products
#### http://domain:8080/showtime/services/products


### GET: get product by id
#### http://domain:8080/showtime/services/product/id/{id}


### GET: get product by productId (exturnal id)
#### http://94.214.159.219:7575/showtime/services/product/pid/{pid}


### GET: get product by ean (europe article number)
#### http://domain:8080/showtime/services/product/ean/{ean}


### GET: search for a product with title or part of a title (case insensitive)
#### http://domain:8080/showtime/services/product/search/{title}


### GET: search for a product with year and title or part of a title (case insensitive)
#### http://domain:8080/showtime/services/product/search/{title}/{year}


### GET: search for a products with title or part of a title (case insensitive)
#### http://domain:8080/showtime/services/products/search/{title}


### GET: search for a products with year title or part of a title (case insensitive)
#### http://domain:8080/showtime/services/products/search/{title}/{year}


### POST: create a new product
#### http://domain:8080/showtime/services/product


### PUT: update a product
#### http://domain:8080/showtime/services/product


### POST: add multiple products at a time
#### http://domain:8080/showtime/services/products


### DELETE: delete a product on id
#### http://domain:8080/showtime/services/product/id/{id}


### DELETE: delete a product on ean (europe article number)
#### http://domain:8080/showtime/services/product/ean/{ean}


### DELETE: delete a product on productId (exturnal id)
#### http://94.214.159.219:7575/showtime/services/product/pid/{pid}